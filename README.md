# PSCEG
## Description
In the network traffic characterization context, a cluster of attacks and a cluster of HTTP flows may exist in different subspaces of the full feature space. Subspace clustering techniques were proposed to discover clusters that exist in relevant feature subsets. PSCEG is a new parallel grid-based subspace clustering algorithm that has good scalability, accuracy and efficiency on massive high-dimensional datasets comparing to existing algorithms. The quality of grid-based subspace clustering results is highly dependent on the grid size and positioning, and many existing methods use sensitive global density thresholds that are difficult to set a priori and will cause a bias to a certain dimensionality. Our algorithm PSCEG can capture the positions of dense units and proper size of the grid based on the data distribution of each dimension without the need to specify its size a priori. To avoid the effect of dimensionality bias, PSCEG conducts subspace clustering with different granularities by defining and using a novel unbiased density estimator that is adaptive to dimensionality.
### Software architecture
The parallelization of PSCEG is based on the Apache Spark using the Resilient Distributed Datasets (RDDs).

PSCEG consists of two main phases: Firstly it generates an exact grid with no need to specify the size of the grid a priori. Each dimension is partitioned into equally sized intervals. After initializing weights, these intervals are used as input data of a special implementation of DBSCAN running in parallel for each dimension on each core. The initialization of density thresholds for the output of DBSCAN is done by a map operation. Only those clusters (units) that are enough dense are stored.

Secondly, it finds subspace clusters of increasing dimensionality in an iterative way using adaptively updated density thresholds. PSCEG first generates and then prunes higher dimensional candidate dense units. The density thresholds are recalculated to adapt to the increasing dimensionality. Those having a density higher than the thresholds are recognized as dense units. This iterative process terminates when no more dense units appear.

Details on the specific approach are provided in the papers (see the publication section).
### Parameters
PSCEG requires the following parameters:

* Input filename: name of the input file that contains the input data.

* Output filename: name of the output file where the subspace clustering results will be stored.

* Number of partitions: the number of partitions of the RDD that contains the input data. The higher this parameter is, the higher the level of parallelism is.

* Alpha: a parameter called cluster dominance factor that estimates the density of the expected clusters. 

* Theta: a parameter that regulates epsilon and minPoints for DBSCAN.

## How To
### Platform
PSCEG is implemented using Python 2.7.6 and tested on a 17-node cluster (each with 4 Core(TM) i5 CPUs with 8GB of RAM) using Spark 1.4.1 and Hadoop 2.6.0 in CentOS operating system.
### Input file format
The expected input file is an mxn numeric matrix. Each of the m lines represents an data object of n dimensions, and consists of the object index and a string of n whitespace-separated numbers. For instance:

```0 19.59981 28.21584 20.76166 16.59425 1.6909167774 ...```

```1 19.22174 23.98512 19.7585 27.51049 80.6015194996 ...```

```2 25.07013 20.21528 18.4996 24.0048 55.1228937646 ...```
### Run the program
The project runs using PySpark. Two libraries are needed to successfully run the program: numpy and sklearn.cluster

Before running PSCEG, the textual file containing the input dataset must be copied in the distributed file system (HDFS), e.g.,

hadoop dfs -put example.txt

The command to run the algorithm is as follows:

spark-submit psceg.py `<InputFilePath> <OutputFilePath> <NumPartitions> <alpha> <theta>` e.g.,

spark-submit psceg.py /example.txt outputFile 40 1.5 0.02
### Results and output file format
The results (i.e., the set of subspace clusters) can be found in the HDFS directory `<output_directory>/`. Each paragraph of the output file contains one subspace cluster. Each subspace cluster, with an unique index, is correlated with its subspace, size, and set of data object indexes. Each paragraph in the output file is in the form:

(clusterIndex,(subspace[dimension1,dimension2,...], sizeOfCluster, setOfDataObjects[object1,object2,...])) e.g.,

(1, ([0, 1, 2, 3], (593, [0, 1, 2, 3, 4, 5, 6, 7, 8,... 597, 598, 599])) represents a cluster in the subspace [0, 1, 2, 3] that contains 593 data objects.
## Examples
An example data set called `example.txt` is available in folder data. Below is an execution example with alpha = 1.5 and theta = 0.02.

spark-submit psceg.py /example.txt outputFile 40 1.5 0.02

The subspace clustering results can be retrieved by viewing text files in the outputFile folder:

cat outputFile/part*
## Publications
The PSCEG algorithm has been submitted to European Symposium on Artificial Neural Networks, Computational Intelligence and Machine Learning (ESANN 2016), April 27-29, 2016, Bruges Belgium
