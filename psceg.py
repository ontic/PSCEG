"""psceg.py"""

from pyspark import SparkContext
from sys import argv 
from time import time
import numpy as np
from sklearn.cluster import DBSCAN
import math


#----------------------------------------------
#		FUNCTIONS
#----------------------------------------------

# Input: partition of RDD string s (a NWpacket with all the features)
# Output: (key,[val]) -> (intRow,floatFeaturesPerRow[])
def torv (s):
	a=s.split(" ")
	b=[]
	for i in range(len(a)-1):
		b.append(float(a[i+1]))
	return (int(a[0]),b)

# Input: (intRow,[floatFeaturesPerRow]) 
# Output: array of (intCol,([intRow],[floatVal]))
def tocrv (tupl):
	tuparray=[]
	for i in range(len(tupl[1])): # for each col number
		tuparray.append((i,([tupl[0]],[tupl[1][i]])))
	return tuparray



# Input: ([Row],[floatVal])
# Output: ([Row],[floatVal])
def approximation(tupl):
        integer = int(tupl[1][0])
        precision = 0.01 * (int((tupl[1][0] - integer) / 0.01))
        
        floatVal = integer * 1.0 + precision
        
        return (tupl[0], [floatVal])





# Input: (key: "floatVal_col", value: count)
# Output: (key: col, value: ([floatVal], [count]))
def histo(tupl):
        array = tupl[0].split("_")
        return (int(array[1]), ([float(array[0])],[tupl[1]]))


# Input: (([floatVal], [count])),(([floatVal], [count]))
# Output: (([floatVal], [count]))
def histolist(tupl1,tupl2):
        t1 = []
        t2 = []
        t1.extend(tupl1[0])
        t1.extend(tupl2[0])
        t2.extend(tupl1[1])
        t2.extend(tupl2[1])

        return (t1,t2)


# Input: ([floatVal], [count])
# Output: a list of CDUs without noise [([start1...],[end1...],[threshold1],[BinIDs])
def histo2CDUs(tupl):

        n = np.matrix(tupl[0])
	m = n.getT()
	result = DBSCAN(eps.value,minP.value).fit_predict(m, sample_weight= tupl[1])

        return (tupl[0], result)


# Input: ([NumByBin],[startVals],[MaxMin],[NumSize])
# Output: countNonZero
def count(tupl):
        co = 0
        for i in range(len(tupl[0])):
            if(tupl[0][i] > 0):
                co += tupl[0][i]
        return co




#(key: dimension(String), values: [start1...],[end1...],[threshold1],[BinIDs],count)

# Input: (key,([start1...],[end1...],[threshold1],[BinIDs],count))
# Output: (key, ([start1...],[end1...],[threshold1],[BinIDs],count))
def isDense(tupl):
    isDense = True
    for i in range(len(tupl[1][2])):
        if(tupl[1][4] < tupl[1][2][i]):
            isDense = False
            return isDense
    return isDense


# DUlist: [(dimension(String), ([start1...],[end1...],[threshold1],[BinIDs],count)), (dimension(String), ([start1...],[end1...],[threshold1],[BinIDs],count)), ...]
# Input: (dimension(String), ([start1...],[end1...],[threshold1],[BinIDs],count))
# Output: (dimension(String), ([start1...],[end1...],[threshold1],[BinIDs]))
def newgenerateCDUs(tupl):
    array = []
    ds1 = tupl[0].split(",")
    for DU in DUlist.value:
        ds2 = DU[0].split(",")
        proper = True
        if(int(ds1[-1]) >= int(ds2[-1])):
            continue
        for i in range(len(ds1) - 1):
            if(ds1[i] != ds2[i]):
                proper = False
                break
        if(proper):
            for i in range(len(ds1)-1):
                if(tupl[1][0][i] != DU[1][0][i]):#to check if they have k-1 same bins, if the id of the corresponding bins do not match, continue
                    continue


            #make a copy of tupl and insert new elements
            t1 = []
            for i in range(len(tupl[1][0])):
                t1.append(tupl[1][0][i])
            t1.append(DU[1][0][-1])
            t2 = []
            for i in range(len(tupl[1][1])):
                t2.append(tupl[1][1][i])
            t2.append(DU[1][1][-1])
            t3 = []
            for i in range(len(tupl[1][2])):
                t3.append(tupl[1][2][i])
            t3.append(DU[1][2][-1])
            t4 = []
            for i in range(len(tupl[1][3])):
                t4.append(tupl[1][3][i])
            t4.append(DU[1][3][-1])
              
            dimensions = tupl[0] + "," + ds2[-1]
            
            array.append((dimensions,(t1,t2,t3,t4)))

    return array
                   

# DUlist: [(K-1_dimension(String), ([start1...],[end1...],[threshold1],[BinIDs],count)), (K-1_dimension(String), ([start1...],[end1...],[threshold1],[BinIDs],count)), ...]    
# Input: (K_dimension(String), ([start1...],[end1...],[threshold1],[BinIDs])) 
# Output: (K_dimension(String), ([start1...],[end1...],[threshold1],[BinIDs]))  should be pruned or not
def pruning(tupl):
    DUwholelist = []
    for DU in DUlist.value:
        ds1 = DU[0].split(",")
        DUset = []
        for i in range(len(ds1)):
            dim_id = ds1[i] + "_" + str(DU[1][3][i])
            DUset.append(dim_id)
        DUwholelist.append(set(DUset))
    CDUtemlist = []
    ds2 = tupl[0].split(",")
    for i in range(len(ds2)):
        dim_id = ds2[i] + "_" + str(tupl[1][3][i])
        CDUtemlist.append(dim_id)
    CDUset = set(CDUtemlist)
    
    k = len(ds2)
    count = 0
    for i in range(len(DUwholelist)):
        if(CDUset.issuperset(DUwholelist[i])):
            count = count + 1

    if(count == k):
        return True
    else:
        return False            

#Input: (key: dimension(String), values: ([start1...],[end1...],[threshold1],[BinIDs]))
#Output: (key: dimension_IDs(String), values: ([start1...],[end1...],[threshold1],[BinIDs]))
def dims_ids(tupl):
    key = tupl[0]
    for i in range(len(tupl[1][3])):
        key = key + "_" + str(tupl[1][3][i])
    return (key,tupl[1])


# Input: ([start1...],[end1...],[threshold1],[BinIDs]) ([start1...],[end1...],[threshold1],[BinIDs])
# Output: ([start1...],[end1...],[threshold1],[BinIDs])
def redundant(tupl1, tupl2):
    return tupl1


# Input: (key: dimension(String), values: ([start1...],[end1...],[threshold1],[BinIDs]))
# Output: (key: 1, values: [([dimensions],[start1...],[end1...],[threshold1],[BinIDs])])
def mergeAllCDU(tupl):
    t = []
    s = tupl[0].split(",")
    for i in range(len(s)):
        t.append(int(s[i]))
    return (1, [(t,tupl[1][0],tupl[1][1],tupl[1][2],tupl[1][3])])

# Input: [([dimensions],[start1...],[end1...],[threshold1],[BinIDs])], [([dimensions],[start1...],[end1...],[threshold1],[BinIDs])]
# Output: [([dimensions],[start1...],[end1...],[threshold1],[BinIDs]),([dimensions],[start1...],[end1...],[threshold1],[BinIDs])]
def allCDU(list1,list2):
    t = []
    for i in range(len(list1)):
        t.append(list1[i])
    for i in range(len(list2)):
        t.append(list2[i])
    return t



# CDUlist: [([dimensions],[start1...],[end1...],[threshold1],[BinIDs]), (...),...]
# Input: ([floatVal])
# Output: [(CDUid1,1,floatValforitsDims[]),(CDUid2,1,floatValforitsDims[]),...]         
def pCount(tupl):
    CDUs = CDUlist.value
    t = []
    
    for i in range(len(CDUs)):
        flag = True
        floatValforitsDims = []
        for j in range(len(CDUs[i][0])):
            dim = CDUs[i][0][j]
            start = CDUs[i][1][j]
            end = CDUs[i][2][j]
            floatValforitsDims.append(tupl[dim])
            if(tupl[dim] < start or tupl[dim] > end):
                flag = False
        if(flag):
            t.append((i,1,floatValforitsDims))
    return t


# Input: (pointID, [(CDUid1,1,floatValforitsDims[]),(CDUid2,1,floatValforitsDims[]),...])
# Output: [(CDUid,(1,floatValforitsDims[],floatValforitsDims[]))...]
def ptoCDU(tupl):
    CDUcount = []
    for i in range(len(tupl[1])):
            CDUcount.append((tupl[1][i][0],(tupl[1][i][1],tupl[1][i][2],tupl[1][i][2])))
    return CDUcount


# Input: (1,floatValforitsDims[],floatValforitsDims[]),(1,floatValforitsDims[],floatValforitsDims[])
# Output: (1,MaxVals[],MinVals[])
def sumupcountandMaxmin(tupl1,tupl2):
    sumup = tupl1[0] + tupl2[0]
    MaxVals = []
    MinVals = []
    for i in range(len(tupl1[1])):
        if(tupl1[1][i] > tupl2[1][i]):
            MaxVals.append(tupl1[1][i])
            
        else:
            MaxVals.append(tupl2[1][i])

        if(tupl1[2][i] < tupl2[2][i]):
            MinVals.append(tupl1[2][i])
        else:
            MinVals.append(tupl2[2][i])

    return (sumup,MaxVals,MinVals)



# CDUlist: [([dimensions],[start1...],[end1...],[threshold1],[BinIDs]), (...),...]
# Input: (key: CDUid, value: (count,MaxVals[],MinVals[]))
# Output: (key: dimension(String), value: ([newstart1...],[newend1...],[newthreshold1],[BinIDs],count))
def allCDUwithCountupdated(tupl):
    CDUs = CDUlist.value
    CDU = CDUs[tupl[0]]
    key = ""
    for i in range(len(CDU[0])-1):
        key += (str(CDU[0][i]) + ",")
    key += str(CDU[0][len(CDU[0])-1])

    newthreshold = []
    for i in range(len(CDU[0])):
        di = 100
        r = tupl[1][1][i] - tupl[1][2][i]
        if(r == 0):
            newthreshold.append(numberOfPoints)
        else:
            newthreshold.append(alpha * r * numberOfPoints * 1.0 / di )        
    
    return (key, (tupl[1][2],tupl[1][1],newthreshold,CDU[4],tupl[1][0]))




# newDUlist: [(key: dimensions (string), values: ([start1...], [end1...], [thresholds1...], [BinIDs...], count)),(key: dimensions (string), values: ([start1...], [end1...], [thresholds1...], [BinIDs...], count)), (...)]
# Input: (key: dimensions (string), values: ([start1...], [end1...], [thresholds1...], [BinIDs...], count))
# Output: (key: dimensions (string), values: ([start1...], [end1...], [thresholds1...], [BinIDs...], count))
def isUnJoined(tupl):
    isjoined = False
    dimensions = tupl[0].split(",")
    allcorrect = False

    for i in range(len(newDUlist.value)):
        newDimensions = newDUlist.value[i][0].split(",")
        diff = list(set(newDimensions) - set(dimensions))
        if(len(diff) != 1):
            continue
        else:
            allcorrect = True
            for j in range(len(dimensions)):
                index2 = newDimensions.index(dimensions[j])
                if(tupl[1][3][j] != newDUlist.value[i][1][3][index2]):
                    allcorrect = False
        isjoined = allcorrect
        if(isjoined):
            break

    isunjoined = not isjoined
    return isunjoined


#for the generation of clusters with pointIDs

# Input: (key: dimension(String), values: ([start1...],[end1...],[threshold1],[BinIDs],clusterID))
# Output: (key: 1, values: [([dimensions],[start1...],[end1...],[threshold1],[BinIDs],clusterID)])
def mergeAllCDUfinal(tupl):
    t = []
    s = tupl[0].split(",")
    for i in range(len(s)):
        t.append(int(s[i]))
    return (1, [(t,tupl[1][0],tupl[1][1],tupl[1][2],tupl[1][3],tupl[1][4])])


# Input: [([dimensions],[start1...],[end1...],[threshold1],[BinIDs],clusterID)], [([dimensions],[start1...],[end1...],[threshold1],[BinIDs],clusterID)]
# Output: [([dimensions],[start1...],[end1...],[threshold1],[BinIDs],clusterID),([dimensions],[start1...],[end1...],[threshold1],[BinIDs],clusterID)]
def allCDUfinal(list1,list2):
    t = []
    for i in range(len(list1)):
        t.append(list1[i])
    for i in range(len(list2)):
        t.append(list2[i])
    return t




# CDUlist: [([dimensions],[start1...],[end1...],[threshold1],[BinIDs],clusterID), (...),...]
# Input: (key: pointID, value: [floatVal])
# Output: [key: pointID, value: clusterIDs[]]
def getPointIds(tupl):
    CDUs = CDUlist.value
    t = []
    for i in range(len(CDUs)):
        flag = True
        for j in range(len(CDUs[i][0])):
            dim = CDUs[i][0][j]
            start = CDUs[i][1][j]
            end = CDUs[i][2][j]
            if(tupl[1][dim] < start or tupl[1][dim] >= end): 
                flag = False
        if(flag):
            t.append(CDUs[i][5])
    return (tupl[0],t)

# Input: [key: pointID, value: clusterIDs[]]
# Output: an array of ([key: clusterID, value: pointID[]])
def clusterPointID(tupl):
    array = []
    for i in range(len(tupl[1])):
        array.append((tupl[1][i],[tupl[0]]))
    return array



# Input: (pointID[]),(pointID[])
# Output: (pointIDs[])
def clusterPointIDs(tupl1,tupl2):
    array = []
    array.extend(tupl1)
    array.extend(tupl2)
    return array



# CDUlist: [([dimensions],[start1...],[end1...],[threshold1],[BinIDs],clusterID), (...),...]
# Input: (key: clusterID, value: pointIDs[])
# Output: (key: clusterID, value: ([dimensions],[start1...],[end1...],pointIDs[]))
def allclusterwithPoints(tupl):
    CDUs = CDUlist.value
    array = []
    for i in range(len(CDUs)):
        if(CDUs[i][5] == tupl[0]):
            array.append(CDUs[i])

    return (tupl[0], (array[0][0], len(tupl[1]), tupl[1]))


# Input: (floatROWS[], floatVALS[])
# Output: (floatVALS[],result[])
def dbscan (tupl):
        n = np.matrix(tupl[1])
	m = n.getT()
	result = DBSCAN(eps.value,minP.value).fit_predict(m)
	lr = len(result)
	if lr != 0:
                return (tupl[1],result)


# MaxMin: [(columnID, [max,min]), ..]
# Input: (key: columnID, value:(floatVALS[],result[]))
# Output: (key: columnID, value:((floatVALS[],result[]),[max,min]))
def addmaxmin(tupl):
        return (tupl[0], (tupl[1],[100, 0]))


# Input: ((floatVALS[],result[]),[max,min])
# Output: #a list of CDUs without noise [([start1...],[end1...],[threshold1],[BinIDs]), (...)] 
def dbscan2CDUlist (tupl):
        val = []
        #Pids = []
        maximumGlobal = -100000
        minimumGlobal = 100000
        for i in range(max(tupl[0][1]) + 1):
            clu = []
            maximum = -100000
            minimum = 100000
            count = 0
            for j in range(len(tupl[0][1])):
                if(tupl[0][1][j] == i):
                    count += 1
                    if(maximum < tupl[0][0][j]):
                        maximum = tupl[0][0][j]
                    if(minimum > tupl[0][0][j]):
                        minimum = tupl[0][0][j]
            clu.append([minimum])
            clu.append([maximum])
            clu.append([i])
            clu.append(count)
            val.append(clu)
            if(maximumGlobal < maximum):
                maximumGlobal = maximum
            if(minimumGlobal > minimum):
                minimumGlobal = minimum


        #sort CDU list according to start value
        startVal = []
        endVal = []
        for i in range(len(val)):
            startVal.append(val[i][0][0])
            endVal.append(val[i][1][0])

        start = sorted(startVal)
        end = sorted(endVal)
        count = max(tupl[0][1]) + 1

        for i in range(len(start)):
            clu = []
            if(start[i] != tupl[1][1] and i == 0):
                clu.append([tupl[1][1]])
                clu.append([start[0]])
                clu.append([count])
                clu.append(0)
                val.append(clu)
                count += 1
            elif(end[i] != tupl[1][0] and i == len(start)-1):
                clu.append([end[len(start)-1]])
                clu.append([tupl[1][0]])
                clu.append([count])
                clu.append(0)
                val.append(clu)
                count += 1
            else:
                clu.append([end[i-1]])
                clu.append([start[i]])
                clu.append([count])
                clu.append(0)
                val.append(clu)
                count += 1 

        # confirm there is no interval that has same value for start and end
        newval = []
        for i in range(len(val)):
            if(val[i][0][0] == val[i][1][0]):
                continue
            else:
                newval.append(val[i])             

        # add threshold to each interval
        value = []
        for i in range(len(newval)):
            maximum = newval[i][1][0]
            minimum = newval[i][0][0]
            threshold = (alpha * (maximum - minimum) * numberOfPoints * 1.0)/ (tupl[1][0]-tupl[1][1]) 
            value.append((newval[i][0],newval[i][1],[threshold],newval[i][2],newval[i][3]))
        return value



# Input: [key: columnID (int), value: [([start1...],[end1...],[threshold1],[BinIDs]), (...)] 
# Output: a list of (key: columnID (String), value:([start1...],[end1...],[threshold1],[BinIDs],count))
def CDUlist2CDUs(tupl):
        array = []
        for i in range(len(tupl[1])):
            array.append((str(tupl[0]),(tupl[1][i])))
        return array


#----------------------------------------------
#               Variables
#----------------------------------------------
if len(argv)!=6:
	print("ERROR: Use -> <thisfile.py> <InputFilePath> <OutputFilePath> <NumPartitions> <alpha> <theta>")
	exit(0)

inputFile = argv[1]
outputFile = argv[2]
numPart = int(argv[3])
alpha = float(argv[4])
theta = float(argv[5])

#----------------------------------------------
#		Main
#----------------------------------------------


sc=SparkContext()
textFile= sc.textFile(inputFile,numPart)

start1 = time()
rv = textFile.map(torv,True).persist() 
numberOfPoints = rv.count()

eps = sc.broadcast(alpha * theta * 100)
minP = sc.broadcast(alpha * theta * numberOfPoints)


window = alpha * theta * 100
densitythreshold = alpha * theta * numberOfPoints

db1 = textFile.map(torv,True).flatMap(tocrv,True).mapValues(approximation).map(lambda (x,y): (str(y[1][0]) + "_"+str(x),1)).reduceByKey(lambda a, b: a + b).filter(lambda (x,y): y >= alpha * numberOfPoints / 10000).map(histo).reduceByKey(histolist).mapValues(histo2CDUs).filter(lambda (x,y): y is not None).persist()

CDU = db1.map(addmaxmin).mapValues(dbscan2CDUlist).flatMap(CDUlist2CDUs).persist()
db1.unpersist()

print("-----------------------")
k = 1 #dimensionality
NDU = CDU.count()
print(NDU)
print("initial part finish")

end1 = time()-start1
print ("Initialization Processed in: " + str(end1))
start2 = time()
#unjoinedDUlist
termDUs = []



#looping part
while(NDU != 0):
    if(k > 1):
        #find-candidate-dense-units----generation and deduplication
        DUlist = sc.broadcast(DU.collect())     
        #this CDU with pruning
        CDU = DU.flatMap(newgenerateCDUs).map(dims_ids).reduceByKey(redundant).map(lambda (x,y): (x.split("_")[0], y)).filter(pruning).persist()
        print("candidates for " + str(k))
        if(CDU.count() == 0):# if there is no candidate generated, add k-1 DUs to termDUs, and stop
            termDUs.append(DU.collect())
            break
    #populate CDUs
    #generate a list for all CDUs and broadcast to all nodes
    CDUs = CDU.map(mergeAllCDU).reduceByKey(allCDU).collect()
    CDUlist = sc.broadcast(CDUs[0][1])
    CDU.unpersist()

    #generate CDU with count after population----point counting
    CDUwithCount = rv.mapValues(pCount).flatMap(ptoCDU).reduceByKey(sumupcountandMaxmin).map(allCDUwithCountupdated)

    #decide whether CDU is dense, and generate DU----dense checking
    newDU = CDUwithCount.filter(isDense).persist()
    NDU = newDU.count()
    print("there are " + str(NDU) + " DUs for Dimensionality " + str(k))

    if(k == 1): #first round pass over directly
        DU = newDU
        k += 1
        continue
    
    #newDUs should be broadcasted
    newDUs = newDU.collect()
    newDUlist = sc.broadcast(newDUs)

    #unjoin check
    unjoinedDUs = DU.filter(isUnJoined)
    termDUs.append(unjoinedDUs.collect())


    DU = newDU
    k += 1

print("looping part finish")
end2 = time()-start2
#print(termDUs)
print ("Looping Processed in: " + str(end2))
start3 = time()

#list all the points that are within each cluster
allclusterswithpointIDs = []
count = -1
for i in range(len(termDUs)):
    for j in range(len(termDUs[i])):
        count = count + 1
        allclusterswithpointIDs.append((termDUs[i][j][0], (termDUs[i][j][1][0], termDUs[i][j][1][1], termDUs[i][j][1][2], termDUs[i][j][1][3], count)))


CDU = sc.parallelize(allclusterswithpointIDs)
CDUs = CDU.map(mergeAllCDUfinal).reduceByKey(allCDUfinal).collect()
CDUlist = sc.broadcast(CDUs[0][1])

#generate CDU with count after population----point counting
finalresult = rv.map(getPointIds).flatMap(clusterPointID).reduceByKey(clusterPointIDs).map(allclusterwithPoints).sortByKey()

finalresult.saveAsTextFile(outputFile)

sc.stop()
end3 = time()-start3
print ("Final result Processed in: " + str(end3))
print ("All Processed in: " + str(end1 + end2 + end3))







